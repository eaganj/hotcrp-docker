#! /bin/bash

if [ $# -ne 2 ]; then
    echo "Usage $0 from to" > /dev/stderr
    exit 1
fi

from="$1"
to="$2"

echo "Cloning $from into $to"

. src/dbutils.sh

E=$(echo "select schema_name from information_schema.schemata where schema_name='$from'" | mysql)

if [ -z "$E" ]; then
    echo "Database $from does not exist" > /dev/stderr
    exit 1
fi

E=$(echo "select schema_name from information_schema.schemata where schema_name='$to'" | mysql)

if ! [ -z "$E" ]; then
    echo "Database $to already exists, drop it" > /dev/stderr
    echo "drop database $to" | mysql
fi

E=$(echo "select * from hotcrp.Conferences where dbname='$site'" | mysql)

if [ -z "$E" ]; then
    echo "Adding $to in hotcrp.Conferences"
    echo "insert into Conferences set dbname='$to'" | mysql hotcrp
else
    echo "$to already in hotcrp.Conferences"
fi

echo "Creating $from"
echo "create database $to" | mysql

echo "Copying $from in $to"
( mysqldump --no-data "$from"; mysqldump --no-create-info "$from" ContactInfo Formula Settings ) | mysql "$to"

#docker-compose exec php /bin/sh -c "php /srv/www/api/utils/set-admin.php -n $to gael.thomas@telecom-sudparis.eu admin"
