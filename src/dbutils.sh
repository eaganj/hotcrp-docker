
. .env

TMP=utils/credentials

echo "[client]" > "$TMP"
echo "password=$MYSQL_ROOT_PASSWORD" >> "$TMP"

chmod 600 "$TMP"

mysql() {
    docker-compose exec -T mysql /bin/sh -c "mysql --defaults-extra-file=/srv/www/api/$TMP $*"
}

mysqldump() {
    str=""
    docker-compose exec -T mysql /bin/sh -c "mysqldump --defaults-extra-file=/srv/www/api/$TMP $*"
}
