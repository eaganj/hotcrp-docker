#! /bin/bash

if [ $# != 1 ]; then
    echo "Usage: $0 site-name" >/dev/stderr
    exit 1;
fi

site="$1"

. src/dbutils.sh


echo "delete r from Roles r inner join Conferences c on r.confid=c.confid where c.dbname='$site'; delete from Conferences where dbname='$site'; drop database $site;" | mysql hotcrp

exit 0
