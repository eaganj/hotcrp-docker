#! /bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: $0 site-name admin-mail1 role-mail1 admin-mail2 role-mail2..." >/dev/stderr
    echo "   role: achair or admin" >/dev/stderr
    exit 1;
fi

site="$1"
shift

echo "======="
echo "======= creating $site with $@ ======="
echo "======="

. src/dbutils.sh

E=$(echo "select schema_name from information_schema.schemata where schema_name='$site'" | mysql)

if [ -z "$E" ]; then
    echo "Database $site does not exist, create it"
    echo "create database $site" | mysql
    cat hotcrp/src/schema.sql | mysql $site
    cat utils/settings.sql | mysql $site
else
    echo "Database $site already exists, skipping creation"
fi

E=$(echo "select * from hotcrp.Conferences where dbname='$site'" | mysql)

if [ -z "$E" ]; then
    echo "Adding $site in hotcrp.Conferences"
    echo "insert into Conferences set dbname='$site'" | mysql hotcrp
else
    echo "$site already in hotcrp.Conferences"
fi

while [ $# -ne 0 ]; do
    mail=$1
    role=$2
    echo "Setting $role for $mail in $site"
    docker-compose exec php /bin/sh -c "php /srv/www/api/utils/set-admin.php -n $site $mail $role"
    shift
    shift
done

exit 0
