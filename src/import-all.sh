#! /bin/bash

prev=21
year=22
import=3

#./src/new-site.sh phd$year gael.thomas@telecom-sudparis.eu achair
#./src/new-site.sh csn$year gael.thomas@telecom-sudparis.eu admin    stephane.maag@telecom-sudparis.eu chair      natalia.kushik@telecom-sudparis.eu chair
#./src/new-site.sh cps$year gael.thomas@telecom-sudparis.eu admin    sergio.mover@polytechnique.edu chair
#./src/new-site.sh cyber$year gael.thomas@telecom-sudparis.eu admin  olivier.levillain@telecom-sudparis.eu chair
#./src/new-site.sh dataai$year gael.thomas@telecom-sudparis.eu admin goran.frehse@ensta-paristech.fr chair        louis.jachiet@telecom-paris.fr chair
#./src/new-site.sh fcs$year gael.thomas@telecom-sudparis.eu admin    samuel.mimram@lix.polytechnique.fr chair
#./src/new-site.sh hpda$year gael.thomas@telecom-sudparis.eu admin   francois.trahay@telecom-sudparis.eu chair
#./src/new-site.sh igd$year gael.thomas@telecom-sudparis.eu admin    eric.lecolinet@telecom-paristech.fr chair    eagan@enst.fr chair
#./src/new-site.sh mpro$year gael.thomas@telecom-sudparis.eu admin   sourour.elloumi@ensta-paris.fr chair
#./src/new-site.sh pds$year gael.thomas@telecom-sudparis.eu achair   pierre.sutra@telecom-sudparis.eu chair

for track in csn cps cyber dataai fcs hpda igd mpro pds; do
#for track in phd csn cps cyber dataai fcs hpda igd mpro pds; do
#    ./src/clone.sh $track$prev $track$year
    ./src/import.sh $track $year import$import "$@"
done

exit 0

