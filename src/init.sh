#! /bin/bash

. src/dbutils.sh

E=$(echo "select schema_name from information_schema.schemata where schema_name='hotcrp'" | mysql)

if [ -z "$E" ]; then
    echo "create database hotcrp" | mysql
    cat hotcrp/test/cdb-schema.sql | mysql hotcrp
else
    echo "hotcrp database already initialized"
fi

exit 0

