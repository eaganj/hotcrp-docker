# Installation

In order to install hotcrp, you have to run:
```
git clone ssh://git@gitlab.inf.telecom-sudparis.eu:2222/inf-sysadmin/hotcrp-docker.git
cd hotcrp-docker
cp src/.env.default .env
```

At this step, you may want to edit `.env`. After, you have to start hotcrp:

```
git clone https://github.com/kohler/hotcrp.git
docker-compose up -d
# here you have to wait: the script will only work when the container will be correctly started
./src/init.sh
```

# Creating a hotcrp site

For that, you just have to run `./src/new-site.sh`. Th first argument is the name
of the hotcrp, the second one the mail of the administrator, and the third one the role
of the administrator (achair = chair + admin, admin = only admin).
For example, if you want to create the `pds21` hotcrp, you just have to run:

```
./src/new-site.sh pds21 gael.thomas@telecom-sudparis.eu achair
```

At this step, your site is up and rechable at `https://cs.ip-paris.fr/hotcrp/pds21`.

# Importing applications from the IP Paris portal

If you want to import applications from the IP Paris portal, you have to copy the configuration file provided
in import:
```
cp utils/import-options.sample.php utils/import-options.php
```

Then, you have to edit this file. Finally, you just have to run the import script
to import the applications in your hotcrp instance.
The script automatically deduces which applications you want in which instance
(e.g., applications to the PhD tracks for phd21, to the HPDA track for hpda21 etc.).
For example, to import the PDS applications in `pds21`:

```
./src/import.sh pds 21 import3
```

If you want to populate all the databases, just run:
```
./src/import-all.sh import3
```

# Exporting the decisions to the IP Paris portal

:warning: **WARNING! Use this script at your own risk!**

## Preparing the script
By default, if you reject an application, I will say that the reason is "Résultats académiques insuffisants".
If you want something better, you can add reject decisions named:
* MPR for "Manque de prérequis récent"
* PI for "Profil inadapté"
* CAP for "Capacité d'accueil dépassée"
* RAI for "Résultats académiques insuffisants"
* OTHER for "Affiliation administrative dans un autre établissement pour ce programme"

To create a decision, in hotcrp, in `Settings->Decisions` you can add a named decision. Don't forget to mark that it's a reject decision.
If you already have your own decision, you can just rename them in the settings to match my names :)
Or you can modify my script (see below) to match tour names.
But it's more risky.

Personally, I don't use this feature and it was not yet tested. But it should work.

## Using the script

First, download the json of your track. For that, click on the "Search" button. Then, at the end of the page: `download->Paper information->json`. At this step, you should have a file like "pds22-data.json".

If not yet done:
```
cp utils/config.sample.php utils/config.php
```

And modify your login/password.

Finally, first do a dry run by setting `export` to `false` in `utils/options.php`, and run the script:
```
php utils/export.php pds22-data.json
```

And everything went find, change `export` to `true` in `utils/options.php``and re-run the same script.
