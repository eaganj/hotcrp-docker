<?php

if($argc < 5) {
  fprintf(STDERR, "Usage: php %s -n conf email role\n", $argv[0]);
  exit(1);
}

$email = $argv[3];

require_once(preg_replace('/\/utils\/[^\/]+/', '/src/siteloader.php', __FILE__));
define("HOTCRP_OPTIONS", SiteLoader::find("conf/options.php"));
define("HOTCRP_TESTHARNESS", true);
ini_set("error_log", "");
ini_set("log_errors", "0");
ini_set("display_errors", "stderr");
ini_set("assert.exception", "1");

require_once(SiteLoader::find("src/init.php"));

switch($argv[4]) {
  case 'admin': $role = Contact::ROLE_ADMIN; break;
  case 'achair': $role = Contact::ROLE_PC | Contact::ROLE_CHAIR | $role = Contact::ROLE_ADMIN; break;
  case 'chair': $role = Contact::ROLE_PC | Contact::ROLE_CHAIR; break;
  case 'pc': $role = Contact::ROLE_PC; break;
  default: fprintf(STDERR, "Unknow role: %s\n", $argv[4]); exit(1);
}

if(!Contact::create($Conf, null, [ "email" => $email ],
                    Contact::SAVE_ANY_EMAIL | Contact::SAVE_IMPORT | Contact::SAVE_ROLES,
                    $role)) {
  echo "  Unable to create " . $email . "\n";
}

?>
