-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: hotcrp
-- ------------------------------------------------------
-- Server version	8.0.26

--
-- Dumping data for table `Settings`
--

LOCK TABLES `Settings` WRITE;

delete from `Settings` where name='allowPaperOption';
delete from `Settings` where name='extrev_chairreq';
delete from `Settings` where name='extrev_view';
delete from `Settings` where name='outcome_map';
delete from `Settings` where name='review_form';
delete from `Settings` where name='tag_chair';
delete from `Settings` where name='pcrev_any';
delete from `Settings` where name='setupPhase';

/*!40000 ALTER TABLE `Settings` DISABLE KEYS */;
INSERT INTO `Settings` VALUES
       (_binary 'allowPaperOption',248,NULL),
       (_binary 'cap_key',1,_binary '5BPSJyRp88Cu6BQeg74CGA=='),
       (_binary 'cmt_always',1,NULL),
       (_binary 'decisions',1,_binary '[{\"id\":-2,\"category\":\"r\",\"name\":\"Reject\"},{\"id\":\"new\",\"category\":\"r\",\"name\":\"Math\",\"suffix\":\"_n1\"},{\"id\":4,\"category\":\"a\",\"name\":\"Strong accept\"},{\"id\":5,\"category\":\"a\",\"name\":\"Accept\"},{\"id\":6,\"category\":\"a\",\"name\":\"Weak accept\"}]'),
       (_binary 'extrev_chairreq',-1,NULL),
       (_binary 'extrev_view',2,NULL),
       (_binary 'msg.home',1,_binary '<h2>Welcome to the hotcrp used for applications in CS of Institut Polytechnique de Paris!</h2>\n\n<p>For the track chairs, you are in charge of assigning reviewers to the students that belong to your track.</p>\n\n<p>We will intensively use the tag system during the whole evaluation. You can use the following tags:</p>\n<ul>\n<li>phddataai, phdcs: if the students applied in PhD track</li>\n<li>math: for a  student that applied for the PhD track in DataAI, but for the master in math. Has to be manually filled for the PhD track students</li>\n<li>csn, cps, cyber, dataai, fcs, mpro, hpda, pds, igd: to assign a student to a track for the students in cs. Has to be manually filled for the PhD track students</li>\n<li>m1, m2: application level (m1 by default for the students that applied for a PhD track, in this case, has to be manually modified)</li>\n</ul>\n\n<p>In order to use tags in the search bar:</p>\n<ul>\n<li>#tag1 #tag2: all the students with the tags tag1 and tag2. For example: #pds => give the students in the pds track</li>\n<li>!#tag: invert the request, for example: !#pds => gives all the students that are not in the pds track</li>\n</ul>\n\n'),
       (_binary 'opt.noAbstract',1,NULL),
       (_binary 'opt.noPapers',1,NULL),
       (_binary 'options',24,_binary '[{\"id\":1,\"name\":\"All applications\",\"type\":\"text\",\"position\":1,\"description\":\"\",\"display\":\"prominent\"},{\"id\":3,\"name\":\"Nationality\",\"type\":\"text\",\"position\":2,\"description\":\"\",\"display\":\"prominent\"},{\"id\":5,\"name\":\"Id IPP\",\"type\":\"text\",\"position\":3,\"description\":\"\",\"display\":\"prominent\"},{\"id\":6,\"name\":\"Documents\",\"type\":\"attachments\",\"position\":4,\"description\":\"\",\"display\":\"topics\"}]'),
       (_binary 'outcome_map',1,_binary '{\"-2\":\"Reject\",\"-1\":\"Math\",\"4\":\"Strong accept\",\"5\":\"Accept\",\"6\":\"Weak accept\"}'),
       (_binary 'paperacc',0,NULL),
       (_binary 'papermanager',0,NULL),
       (_binary 'pc_seeall',1,NULL),
       (_binary 'pc_seeallpdf',1,NULL),
       (_binary 'pc_seeallrev',1,NULL),
       (_binary 'rev_ratings',2,NULL),
       (_binary 'rev_roundtag',1,_binary 'Review'),
       (_binary 'review_form',1,_binary '[{\"id\":\"s05\",\"name\":\"Overall merit\",\"description\":\"<p>For your evaluation, you should consider that any level A, B or C student has to be accepted, while a level D or E student does not. For the PhD track students:<\\/p>\\n<ul>\\n<li>For a level A student, you consider that giving a PhD grant is important,<\\/li>\\n<li>For a level B student, you consider that giving a life grant (only during the master) is important,<\\/li> \\n<li>For a level C student, you consider that having the student in the program is important, but that a grant is optional.<\\/li>\\n<\\/ul>\",\"position\":1,\"visibility\":\"pc\",\"options\":[\"Reject\",\"Weak reject\",\"Weak accept\",\"Accept\",\"Strong accept\"],\"option_letter\":\"A\",\"required\":true},{\"id\":\"s02\",\"name\":\"Level of the university\",\"description\":\"<p>Optional. At least one reviewer should rank the university. Note that the best universities of any country have to be considered as top universities.<\\/p>\",\"position\":2,\"visibility\":\"pc\",\"options\":[\"Bad\",\"Medium\",\"Top\"],\"option_letter\":\"A\",\"required\":false},{\"id\":\"s03\",\"name\":\"Academic level\",\"position\":3,\"visibility\":\"pc\",\"options\":[\"Bad\",\"Medium\",\"Good\",\"Very good\"],\"option_letter\":\"A\",\"required\":true},{\"id\":\"s04\",\"name\":\"Motivation letter\",\"position\":4,\"visibility\":\"pc\",\"options\":[\"Not convincing\",\"Convincing\",\"Amazing\"],\"option_letter\":\"A\",\"required\":true},{\"id\":\"s06\",\"name\":\"Suggest another track\",\"description\":\"<p>Only fill this field if the track of the students does not seem adequate.<\\/p>\",\"position\":5,\"visibility\":\"pc\",\"options\":[\"Computer Science for Networks (CSN)\",\"Cyber-Physical Systems (CPS)\",\"Cybersecurity (Cyber)\",\"Data and Artificial Intelligence (DataAI)\",\"Foundations of Computer Science (FCS)\",\"High Performance Data Analytics (HPDA)\",\"Interaction, Graphics & Design (IGD)\",\"Operations Research (MPRO)\",\"Parallel and Distributed Systems (PDS)\"],\"option_class_prefix\":\"sv-viridis\",\"required\":false},{\"id\":\"t01\",\"name\":\"Suggest a tutor\",\"description\":\"<p>Optional. For the PhD track students, at least one of the reviewers should suggest a name. Please, give the name, the email and the laboratory of the tutor.<\\/p>\",\"position\":6,\"visibility\":\"pc\"},{\"id\":\"t02\",\"name\":\"Application\'s comment\",\"description\":\"<p>Mandatory. Here, explain your overall merit grade.<\\/p>\",\"position\":7,\"visibility\":\"pc\"}]'),
       (_binary 'seedec',3,NULL),
       (_binary 'sub_blind',0,NULL),
       (_binary 'sub_freeze',0,NULL),
       (_binary 'sub_pcconfsel',1,NULL),
       (_binary 'sub_pcconfvis',1,NULL),
       (_binary 'tag_chair',1,_binary 'accept cps csn cyber dataai fcs hpda igd m1 m2 math mpro pds phdcs phddataai reject'),
       (_binary 'tag_color',1,_binary 'cps=orange fcs=orange dataai=yellow igd=yellow pds=green hpda=green cyber=blue csn=blue mpro=purple fcs=italic igd=italic hpda=italic csn=italic accept=underline reject=strikethrough math=strikethrough math=dim'),
       (_binary 'tag_rounds',1,_binary 'Review'),
       (_binary 'tag_seeall',1,NULL),
       (_binary 'pcrev_any',0,NULL),
       (_binary 'rev_open',1640200531,NULL),
       (_binary 'pcrev_informtime',1640200531,NULL),
       (_binary 'pcrev_assigntime',1640200531,NULL),
       (_binary 'topics',1,_binary '[]'),
       (_binary 'tracker',0,_binary '{\"trackerid\":false,\"position_at\":1634681319.434463,\"update_at\":1634681319.434463}');
/*!40000 ALTER TABLE `Settings` ENABLE KEYS */;
UNLOCK TABLES;

