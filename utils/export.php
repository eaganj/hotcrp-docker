<?php

require_once(dirname(__FILE__) . "/login.php");

if($argc < 2) {
  fprintf(STDERR, "Usage: %s json\n", $argv[0]);
  exit(1);
}

if(!file_exists($argv[1])) {
  fprintf(STDERR, "unable to find %s\n", $argv[1]);
  exit(1);
}

$records = json_decode(file_get_contents($argv[1]));

$reasons = array("A"     => array("Accept", "ADM", "DAI"),
                 "LC"    => array("Liste complémentaire", "LCO", "DAI"),
                 "MPR"   => array("Manque de prérequis récent", "REF", "MPR"),
                 "PI"    => array("Profil inadapté", "REF", "PI"),
                 "CAP"   => array("Capacité d'accueil dépassée", "REF", "CAP"),
                 "RAI"   => array("Résultats académiques insuffisants", "REF", "DAI"),
                 "OTHER" => array("Affiliation administrative dans un autre établissement pour ce programme", "REF", "AAP"));

$ch = ipp_login();

curl_setopt($ch, CURLOPT_URL, $urls["json"]);
$json = json_decode(curl_exec($ch));

function lookup_json($json, $cand) {
  foreach($json->data as $cur)
    if($cur->ID_CANDIDATURE == $cand)
      return $cur;
  return null;
}

$max = 0;
$total = 0;

foreach($records as $record) {
  if($record->status != "submitted") {
    $cur = lookup_json($json, $record->id_cand);

    $both = strstr($record->all_applications, "M1") && strstr($record->all_applications, "M2");

    if(!$cur) {
      fprintf("should not happen: unable to find " . $record->id_cand . " in json");
      exit(1);
    } else if(!empty($cur->FLAG_ETAT_GLOBAL)) {
      $mismatch = ($cur->FLAG_ETAT_GLOBAL == "Admis" && $record->status == "rejected")
        || ($cur->FLAG_ETAT_GLOBAL == "Refusé" && $record->status == "accepted") ? " (WARNING: MISMATCH)" : "";
      if(!empty($mismatch) && !$both)
        llog($record->title . " => already " . $record->status . $mismatch);
    } else {
      $url = $urls["modify"] . "index.php?idcandidature=" . $record->id_cand . "&gid=" . $record->id_ipp;

      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_URL, $url);

      $str_dom = curl_exec($ch);
      if(!$str_dom) {
        fprintf(STDERR, "unable to download application at " . $url . "\n");
        exit(1);
      } else if($both) {
        llog("SKIPPING " . $record->title . " (" . $record->id_ipp . ", " . $record->status . ") seems to apply both in M1 and M2, you have to manage that manually");
      } else {
        //if($record->id_ipp != 95617)
        //continue;

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($str_dom);

        $form = $dom->getElementById('formpresidentForm');
        $action = $urls["modify"] . $form->getAttribute("action");
        //print_r($action);

        $tag = $record->status == "accepted" ? "A" : (array_key_exists($record->decision, $reasons) ? $record->decision : "RAI");
        $reason = $reasons[$tag];

        $data = array("choixPresident" => $reason[1],
                      "isPHD" => $dom->getElementById("isPHD")->getAttribute("value"),
                      "choixPresidentRefus" => $reason[2],
                      "token_juryipparis" => $dom->getElementById("token_juryipparis")->getAttribute("value"));

        //print_r($data);
        llog($record->title . " => " . $reason[0] . " (" . $record->status . ")");

        if($options["export"]) {
          curl_setopt($ch, CURLOPT_URL, $action);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
          curl_exec($ch);
        }
      }
    }
  }

  if(++$total == $max) {
    llog("** stop after processing " . $total . " records **");
    break;
  }
}

?>
