<?php

require_once(dirname(__FILE__) . "/login.php");
require_once(dirname(__FILE__) . "/monmaster/process.php");

if($argc < 3) {
  fprintf(STDERR, "Usage: %s track year tags\n", $argv[0]);
  fprintf(STDERR, "   for example, %s pds 22 import2\n", $argv[0]);
  exit(1);
}

$track = $argv[1];
$year = $argv[2];
$default_tags = array_slice($argv, 3);

$argc = 3;
$argv[1] = '-n';
$argv[2] = $track . $year;

require_once(preg_replace('/\/utils\/[^\/]+/', '/src/siteloader.php', __FILE__));
define("HOTCRP_OPTIONS", SiteLoader::find("conf/options.php"));
define("HOTCRP_TESTHARNESS", true);
ini_set("error_log", "");
ini_set("log_errors", "0");
ini_set("display_errors", "stderr");
ini_set("assert.exception", "1");

require_once(SiteLoader::find("src/init.php"));

$confid = $Opt['confid'];

if(!$confid) {
  fprintf(STDERR, "providing the confid is mandatory\n");
  exit(1);
}

llog("=======");
llog("======= importing applications in $confid =======");
llog("======= with tags " . implode(", ", $default_tags) . " ========");
llog("=======");

$base_confid = str_replace($year, '', $confid);

$ps = new PaperStatus($Conf);

#
#
# load the json
#
#
$ch = ipp_login();
curl_setopt($ch, CURLOPT_URL, $urls["json"]);
$json = json_decode(curl_exec($ch));

if($options["show_json"]) {
  print_r($json);
  exit(0);
}

#
#
#  cleanup
#
#
if(false && $options["cleanup"]) {
  llog("cleaning the database");
  foreach(array("ActionLog", "Capability", "DocumentLink", "FilteredDocument", "Formula", "MailLog",
                "Paper", "PaperComment", "PaperConflict", "PaperOption", "PaperReview", "PaperReviewPreference",
                "PaperReviewRefused", "PaperStorage", "PaperTag", "PaperTagAnno", "PaperTopic", "PaperWatch",
                "ReviewRating", "ReviewRequest", "TopicInterest") as $cur) {
    $Conf->qe_raw("delete from " . $cur);
  }
  $Conf->qe_raw("ALTER TABLE Paper AUTO_INCREMENT = 1");
}

#
# monmaster
#
$monmaster = monmaster_paths();

#
#
#  
#
#
function formation_info($formation) {
  foreach(array(array('M1 CPS - Cyber Physical Systems', 'M1 CPS', 'cps', 'm1', 'cps'),
                array('M2 CPS - Cyber Physical Systems', 'M2 CPS', 'cps', 'm2', 'cps'),

                array('M1 CSN - Computer Science for Networks', 'M1 CSN', 'csn', 'm1', 'csn'),
                array('M2 CSN - Computer Science for Networks', 'M2 CSN', 'csn', 'm2', 'csn'),

                array('M1 Cyber - Cybersecurity', 'M1 Cyber', 'cyber', 'm1', 'cyber'),
                array('M2 Cyber - Cybersecurity', 'M2 Cyber', 'cyber', 'm2', 'cyber'),

                array('M1 Data AI - Data and Artificial Intelligence', 'M1 DataAI', 'dataai', 'm1', 'dataai'),
                array('M2 Data AI - Data and Artificial Intelligence', 'M2 DataAI', 'dataai', 'm2', 'dataai'),

                array('M1 HPDA - High Performance Data Analytics', 'M1 HPDA', 'hpda', 'm1', 'hpda'),
                array('M2 HPDA - High Performance Data Analytics', 'M2 HPDA', 'hpda', 'm2', 'hpda'),

                array('M1 IGD - Interaction, Graphic and Design', 'M1 IGD', 'igd', 'm1', 'igd'),
                array('M2 IGD - Interaction, Graphic and Design', 'M2 IGD', 'igd', 'm2', 'igd'),

                array('M1 MPRI - Foundations of Computer Science', 'M1 FCS', 'fcs', 'm1', 'fcs'),
                array('M2 MPRI - Foundations of Computer Science', 'M2 FCS', 'fcs', 'm2', 'fcs'),

                array('M1 MPRO - Operation Research', 'M1 MPRO', 'mpro', 'm1', 'mpro'),
                array('M2 MPRO - Operation Research', 'M2 MPRO', 'mpro', 'm2', 'mpro'),

                array('M1 PDS - Parallel and Distributed Systems', 'M1 PDS', 'pds', 'm1', 'pds'),
                array('M2 PDS - Parallel and Distributed Systems', 'M2 PDS', 'pds', 'm2', 'pds'),

                array('PhD Track - Computer Science', 'PhD Track CS', 'phdcs', 'm1', 'phd'),
                array('PhD Track - Data and Artificial Intelligence', 'PhD Track DataAI', 'phddataai', 'm1', 'phd'))
                  as $info) {
    if($formation === $info[0])
      return $info;
  }

  fprintf(STDERR, "Unable to find FORMATION: %s\n", $formation);
  exit(1);
}

function search_json($user, $query, $cols = "id") {
    $pl = new PaperList("empty", new PaperSearch($user, $query));
    $pl->parse_view($cols);
    return $pl->text_json();
}

function id_ipp_to_id_hotcrp($id_ipp) {
  global $Conf;
  foreach(search_json($Conf->root_user(), '', "id_ipp") as $cur) {
    if($id_ipp === $cur['IdIPP'])
      return $cur['id'];
  }
  return null;
}

$db = array();

#
#  download documents
#
function  download_documents($e, $ch, $cur, $info, $suffix, $is_monmaster) {
  global $urls;
  global $options;

  $data_dir = dirname(__FILE__) . '/data';
  if(!file_exists($data_dir))
    mkdir($data_dir);

  $name = $cur->ID . '_' . str_replace(' ', '_', $info[1]) . '.' . $suffix;
  $path = $data_dir . '/' . $name;
  $url_full = $urls[$suffix] . '?telecharger=ok&idcandidature=' . $cur->ID_CANDIDATURE . '&gid=' . $cur->ID;

  if($options["force_download"] || !file_exists($path) || filesize($path) == 0) {
    if($options["no_download"]) {
      llog("  ignoring download: " . $path);
    } else {
      $fh = fopen($path, "w");

      curl_setopt($ch, CURLOPT_URL, $url_full);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
      curl_setopt($ch, CURLOPT_FILE, $fh);

      llog("  downloading: " . $path);
    
      if(!curl_exec($ch)) {
        fprintf(STDERR, "unable to download %s\n", $url_full);
        exit(1);
      }

      fclose($fh);
    }
  }

  if(file_exists($data_dir . '/external.' . $name)) {
    llog("    external file exists for " . $path . ", using it");
    system("cp " . $data_dir . '/external.' . $name . ' ' . $path);
  }

  if(file_exists($path)) {
    $doc = new StdClass();
    $doc->filename = ($is_monmaster ? "invalid-" : "") . $name;
    $doc->content_file = str_replace("/srv/www/api/", "", $path);
    array_push($e->json->documents, $doc);
  }
}


#
# pre processing
#
$total = 0;

foreach($json->data as $cur) {
  $id = $cur->ID;
  $first = ucwords(strtolower($cur->PRENOM));
  $last = ucwords(strtolower(trim(str_replace($cur->PRENOM, "", $cur->NOM), ' ')));

// to only process this application:
//  if($cur->ID != 98075)
//    continue;

  //print_r($cur);
//  llog("processing " . $id . " (" . $first . " " . $last . ")");

  if(!array_key_exists($id, $db)) {
    $e = new StdClass();
    $db[$id] = $e;

    $hotcrp_id = id_ipp_to_id_hotcrp($cur->ID);
    if($hotcrp_id) {
      $e->json = $ps->paper_json($hotcrp_id);
      $e->is_an_update = true;
      $e->tags = array();
    } else {
      $e->json = new StdClass();
      $e->json->_id_ = "new";
      $e->is_an_update = false;
      $e->tags = $default_tags;
    }

    $author = new StdClass();
    $author->first = $first;
    $author->last = $last;
    $author->email = $cur->COURRIEL;
    $author->affiliation = $cur->INST_NOM . ", " . $cur->INST_VILLE . ", ". $cur->PAYS_ETAB;
    
    $e->json->id_ipp = $id;
    $e->json->title = $author->first . ' ' . $author->last;
    $e->json->authors = [ $author ];
    $e->json->documents = array();

    $e->json->nationality = $cur->NATIONALITE1 . ($cur->NATIONALITE2 ? ', ' . $cur->NATIONALITE2 : '');

    $e->json->submitted = true;
    $e->json->status = 'submitted';
    $e->json->submitted_at = time();

    $e->applications = array();
    $e->import = false;

    if($cur->TAG == "Mon Master")
      $e->tags[] = "mon-master";
  }

  $info = formation_info($cur->FORMATION);

  array_push($e->applications, $info[1]);
  if($base_confid === $info[4]) {
    llog("preparing " . $e->json->id_ipp);
  
    $e->json->id_cand = $cur->ID_CANDIDATURE;

    array_push($e->tags, $info[2]);
    if(!in_array($info[3], $e->tags))
      array_push($e->tags, $info[3]);

    array_push($e->tags, "wave" . $cur->VAGUE_CAND);

    if(!empty($cur->ID_MONMASTER) && array_key_exists($info[4], $monmaster)) {
      if(!in_array("mon-master", $e->tags))
        $e->tags[] = "mon-master";
      
      $name = $cur->ID_MONMASTER . ".zip";
      $path = $monmaster[$info[4]] . "/" . $name;
      if(file_exists($path)) {
        llog("  using: " . $path);
        $doc = new StdClass();
        $doc->filename = $name;
        $doc->content_file = str_replace("/srv/www/api/", "", $path);
        array_push($e->json->documents, $doc);
      }
    }

    $is_monmaster = count($e->json->documents) > 0;
    
    download_documents($e,
                       $ch,
                       $cur,
                       $info,
                       'zip',
                       $is_monmaster);
    download_documents($e,
                       $ch,
                       $cur,
                       $info,
                       'pdf',
                       $is_monmaster);
    
    $e->import = true;

    if(++$total == $options["max_records"]) {
      llog("*** stopping after " . $total . " records ***");
      break;
    }
  }
}

foreach($db as $e) {
  if(!$options["only_download"] && $e->import) {
    $e->json->all_applications = implode(", ", $e->applications);

    llog(($e->is_an_update ? "updating " : "creating ") . $e->json->id_ipp . " - " . $e->json->id_cand . " (" . $e->json->authors[0]->first . " " . $e->json->authors[0]->last . ") with tags '" . implode($e->tags, " ") . "'");

    $id = $ps->save_paper_json($e->json);

    foreach($e->tags as $tag)
      if($Conf->qe_raw("select * from PaperTag where paperId='" . $id . "' and tag='" . $tag . "'")->num_rows == 0)
        $Conf->qe_raw("insert into PaperTag (paperId, tag, tagIndex) values ('" . $id . "', '" . $tag . "', 0)");
  }
}

?>
