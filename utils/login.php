<?php

require_once(dirname(__FILE__) . "/options.php");

$data_dir = dirname(__FILE__) . '/data';
if(!file_exists($data_dir))
  mkdir($data_dir);

$log = fopen($data_dir . "/log", "a+");

function llog($msg) {
  global $argv;
  global $log;
  fprintf($log, "[%s]: %s\n", $argv[0], $msg);
  printf("[%s]: %s\n", $argv[0], $msg);
}

$url_base = 'https://jury-candidatureipparis.polytechnique.fr/jury_candidatureipparis/';

$urls = array(
  "prepare"   => $url_base . "login/index.php",
  "login"     => $url_base . "login/switch.php",
  "json"      => $url_base . "jury/controllers/candidatures.php",
  "modify"    => $url_base . "modification/",
  "zip"       => $url_base . "genererdocument/datazipper.php",
  "pdf"       => $url_base . "genererdocument/pdfsynthese.php");

function ipp_login() {
  global $urls;
  global $options;
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $urls["prepare"]);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_COOKIESESSION, true);
  curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name');  //could be empty, but cause problems on some hosts
  curl_setopt($ch, CURLOPT_COOKIEFILE, 'tmp');
  //curl_setopt($ch, CURLOPT_VERBOSE, true);

  $str_pre = curl_exec($ch);

  if(!$str_pre) {
    fprintf(STDERR, "Unable to connect to %s: %s\n", $urls["prepare"], curl_error($ch));
    exit(1);
  }

  $dom = new DOMDocument();
  libxml_use_internal_errors(true);
  $dom->loadHTML($str_pre);
  libxml_use_internal_errors(false);
  $token = $dom->getElementById('token_juryipparis')->getAttribute('value');

  curl_setopt($ch, CURLOPT_URL, $urls["login"]);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "login=" . $options["login"] . "&password=" . $options["password"] . "&token_juryipparis=" . $token);

  curl_exec($ch);

  curl_setopt($ch, CURLOPT_POST, false);

  return $ch;
}

?>
