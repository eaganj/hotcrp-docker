<?php

$options = array(
  "login"          => 'gael.thomas',
  "password"       => 'TODO',

  "show_json"      => false, /* true => print the json and exit */
  "max_records"    => 1,     /* 0 means infinite */

  "no_download"    => false, /* don't download, even if the documents do not exist */
  "force_download" => false, /* re download, even if the document do not exist */
  "only_download"  => true,  /* only download and do not update hotcrp */

  "cleanup"        => false, /* true => cleanup the whole database - use with caution! (deactivated in the code) */

  "export"         => false, /* false => run the export script in dry mode */
);

?>
